package cl.acis.miaplicacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private static final String LOG = "MAINACTIVITY";
    private static final String SHARED_PREFERENCES_NAME = "cl.acis.miaplicacion.preferences";

    private static final String EMAIL_VALID = "alumno@duoc.cl";
    private static final String CONTRASENIA_VALID = "duoc.2021";

    private EditText etInputEmail;
    private EditText etInputContrasenia;
    private Button btLogin;
    private CheckBox cbRecuerdame;
    private RadioButton rbSesionMes;
    private RadioButton rbSesionParaSiempre;
    private RadioGroup rgRecordarme;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sync();

        this.preferences = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);

        if(credetialsExists()){
            navigateToOtherActivity(preferences.getString("email", "error"));
        }

        this.cbRecuerdame.setOnClickListener(v -> {
            if(this.cbRecuerdame.isChecked()){
                this.rgRecordarme.setVisibility(View.VISIBLE);
            }else{
                this.rgRecordarme.clearCheck();
                this.rgRecordarme.setVisibility(View.INVISIBLE);
            }
        });

        this.btLogin.setOnClickListener(v -> {
            // obtengo los valores de los inputs
            String email = this.etInputEmail.getText().toString();
            String password = this.etInputContrasenia.getText().toString();
            boolean recordarme = this.cbRecuerdame.isChecked();
            int tiempoRecordarme = this.rgRecordarme.getCheckedRadioButtonId();

            if(email.isEmpty() && password.isEmpty()){
                Toast.makeText(this, "El email y la contraseña no pueden estar vacios",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "El email debe ser valido", Toast.LENGTH_LONG).show();;
                return;
            }

            if(recordarme && tiempoRecordarme == -1){
                Toast.makeText(this, "Debe seleccionar un tiempo para recordar su sesión",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if(email.equals(EMAIL_VALID) && password.equals(CONTRASENIA_VALID)){
                if(recordarme){
                    RadioButton radioButtonSelected = findViewById(tiempoRecordarme);
                    long milisecondMonth = 2592000000L;
                    long tiempoCaducidad = radioButtonSelected.getId() == R.id.rb_sesion_mes ? Calendar.getInstance().getTime().getTime() + milisecondMonth : -1;
                    saveUserSharedPreferences(email, tiempoCaducidad);
                }
                navigateToOtherActivity(email);
            }else{
                Toast.makeText(this, "Credenciales invalidas",
                        Toast.LENGTH_LONG).show();
            }
        });

        this.rbSesionMes.setOnClickListener( v -> {
            RadioButton b = (RadioButton) v;
            Log.e(LOG, "RadioButton Click");
        });

        Log.e(LOG, "onCreate");
    }

    private void navigateToOtherActivity(String email){
        Intent intent = new Intent(this, PokemonActivity.class);
        intent.putExtra("user_email", email);
        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void saveUserSharedPreferences(String userEmail, long tiempoCaducidad){
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("email", userEmail);
        editor.putBoolean("isLogin", true);
        editor.putLong("tiempoCaducidad", tiempoCaducidad);
        editor.apply();
    }

    private boolean credetialsExists(){
        return preferences.getBoolean("isLogin", false);
    }




    /**
     * Metodo que enlaza los widgets con mis atributos
     */
    private void sync(){
        this.etInputEmail = findViewById(R.id.et_input_email);
        this.etInputContrasenia = findViewById(R.id.et_input_contrasenia);
        this.btLogin = findViewById(R.id.bt_login);
        this.cbRecuerdame = findViewById(R.id.cb_recuerdame);
        this.rbSesionMes = findViewById(R.id.rb_sesion_mes);
        this.rbSesionParaSiempre = findViewById(R.id.rb_sesion_para_siempre);
        this.rgRecordarme = findViewById(R.id.rg_recordarme);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(LOG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(LOG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(LOG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(LOG, "onStop");
    }

    @Override
    protected void onDestroy(){
        Log.e(LOG, "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        Log.e(LOG, "onRestart");
        super.onRestart();
    }

}