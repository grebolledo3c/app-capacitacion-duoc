package cl.acis.miaplicacion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cl.acis.miaplicacion.API.API;
import cl.acis.miaplicacion.API.IPokemonService;
import cl.acis.miaplicacion.adapters.PokemonAdapter;
import cl.acis.miaplicacion.models.Pokemon;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PokemonActivity extends AppCompatActivity {
    //solo para el ejemplo
    private static final String SHARED_PREFERENCES_NAME = "cl.acis.miaplicacion.preferences";
    private static final String LOG ="PokemonActivity";

    private RecyclerView rcPokemons;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private SharedPreferences preferences;

    private PokemonSQLiteHelper pokemonHelper;
    private SQLiteDatabase db;

    private List<Pokemon> pokemons = new ArrayList<>();

    private IPokemonService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        this.pokemonHelper = new PokemonSQLiteHelper(this, "PokemonDatabase", null, 1);

        //instancia de lectura y escritura
        this.db = pokemonHelper.getWritableDatabase();
        // instancia solo de lectura
        //this.db = pokemonHelper.getReadableDatabase();

        this.preferences = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);

        this.rcPokemons = findViewById(R.id.rc_pokemons);
        this.layoutManager = new LinearLayoutManager(this);

        this.adapter = new PokemonAdapter(R.layout.pokemon_view_item, this.pokemons, i -> {
            Pokemon p =  this.pokemons.get(i);

            if (existsPokemon(p.getNumeroPokedex())){
                deletePokemon(p.getNumeroPokedex());
            }else{
                createPokemon(p);
            }

        });
        service = API.getApi().create(IPokemonService.class);
        new LoadPokemonsThread().execute(service);

        this.rcPokemons.setLayoutManager(this.layoutManager);
        this.rcPokemons.setAdapter(this.adapter);
        Log.e(LOG, "Aquí se cargo el adapter");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cerrar_sesion:
                removeSharedPreferences();
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.menu_salir:
                finish();
                System.exit(0);
        }

        return super.onOptionsItemSelected(item);
    }

    private void removeSharedPreferences() {
        this.preferences.edit().clear().apply();
    }

    private List<Pokemon> getAllPokemons(){
        Cursor cursor = this.db.rawQuery("SELECT * FROM pokemons", null);

        List<Pokemon> list = new ArrayList<>();

        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                String nombre = cursor.getString(cursor.getColumnIndexOrThrow("nombre"));
                String tipo = cursor.getString(cursor.getColumnIndexOrThrow("tipo"));
                String imagenUrl = cursor.getString(cursor.getColumnIndexOrThrow("imagen_url"));

                Pokemon p = new Pokemon(id, nombre, tipo, imagenUrl);

                list.add(p);

                cursor.moveToNext();
            }
        }

        return list;
    }

    private void createPokemon(Pokemon pokemon){
        if(db != null){
            ContentValues nuevoRegistro = new ContentValues();

            nuevoRegistro.put("id", pokemon.getNumeroPokedex());
            nuevoRegistro.put("nombre", pokemon.getNumeroPokedex());
            nuevoRegistro.put("tipo", pokemon.getTipo());
            nuevoRegistro.put("imagen_url", pokemon.getImagenUrl());


            long resultado = db.insert("pokemons", null, nuevoRegistro);
            if(resultado == -1){
                Toast.makeText(this, "No se ha podido insertar el pokemon",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean existsPokemon(int id){
        Cursor cursor = this.db.rawQuery("SELECT * FROM pokemons where id = ?",
                                                new String[] {String.valueOf(id)});

        return cursor.moveToFirst();
    }

    private void deletePokemon(int id){
        int rows = db.delete("pokemons", "id = ?", new String[] {String.valueOf(id)});
        if( rows > 0){
            Toast.makeText(this, "Pokemon eliminado", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "No se ha podido eliminar el Pokemon", Toast.LENGTH_SHORT).show();
        }
    }

    private Pokemon getPokemonFromApi(int i) throws IOException {
        return service.getPokemon(i).execute().body();
    }

    class LoadPokemonsThread extends AsyncTask<IPokemonService, Integer, List<Pokemon>>{

        @Override
        protected List<Pokemon> doInBackground(IPokemonService... iPokemonServices) {
            for (int i = 1; i <= 10; i++){
                try {
                    pokemons.add(getPokemonFromApi(i));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Pokemon> pokemons) {
            adapter.notifyDataSetChanged();
            super.onPostExecute(pokemons);
        }
    }
}
