package cl.acis.miaplicacion.API;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import cl.acis.miaplicacion.models.Pokemon;

public class PokemonDeserializer implements JsonDeserializer<Pokemon> {
    @Override
    public Pokemon deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        int id = json.getAsJsonObject().get("id").getAsInt();
        String nombre = json.getAsJsonObject().get("name").getAsString();
        //json->types[0]->type->name
        String tipo = json.getAsJsonObject().get("types").getAsJsonArray().get(0).getAsJsonObject()
                        .get("type").getAsJsonObject().get("name").getAsString();
        //json->sprites->front_default
        String imageUrl = json.getAsJsonObject().get("sprites").getAsJsonObject()
                            .get("front_default").getAsString();

        Pokemon p = new Pokemon(id, nombre, tipo, imageUrl);

        return p;
    }
}
