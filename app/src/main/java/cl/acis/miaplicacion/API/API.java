package cl.acis.miaplicacion.API;

import com.google.gson.GsonBuilder;

import cl.acis.miaplicacion.models.Pokemon;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {

    private static final String BASE_URL = "https://pokeapi.co/api/v2/";

    private static Retrofit retrofit = null;

    public static  Retrofit getApi(){
        if(retrofit == null){
            GsonBuilder builder = new GsonBuilder();

            builder.registerTypeAdapter(Pokemon.class, new PokemonDeserializer());

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .build();
        }

        return retrofit;
    }

}
