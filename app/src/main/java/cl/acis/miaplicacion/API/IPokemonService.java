package cl.acis.miaplicacion.API;

import cl.acis.miaplicacion.models.Pokemon;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IPokemonService {

    @GET("pokemon-form/{id}/")
    Call<Pokemon> getPokemon(@Path("id") int id);

}
