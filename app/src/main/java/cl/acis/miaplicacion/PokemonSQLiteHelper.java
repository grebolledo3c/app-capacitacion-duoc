package cl.acis.miaplicacion;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class PokemonSQLiteHelper extends SQLiteOpenHelper {

    String sqlPokemonTable = "CREATE TABLE pokemons(id INTEGER PRIMARY KEY NOT NULL, " +
                                                    "nombre TEXT NOT NULL, " +
                                                    "tipo TEXT NOT NULL, " +
                                                    "imagen_url TEXT)";

    public PokemonSQLiteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(sqlPokemonTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //eliminar la tabla pokemons
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Pokemons");

        //crear la nueva version de la tabla
        sqLiteDatabase.execSQL(sqlPokemonTable);
    }
}
