package cl.acis.miaplicacion.models;

public class Pokemon {
    private int numeroPokedex;
    private String nombre;
    private String tipo;
    private String imagenUrl;
    private boolean stored = false;

    public Pokemon(int numeroPokedex, String nombre, String tipo, String imagenUrl) {
        this.numeroPokedex = numeroPokedex;
        this.nombre = nombre;
        this.tipo = tipo;
        this.imagenUrl = imagenUrl;
    }

    public Pokemon(int numeroPokedex, String nombre, String tipo, String imagenUrl, boolean stored) {
        this.numeroPokedex = numeroPokedex;
        this.nombre = nombre;
        this.tipo = tipo;
        this.imagenUrl = imagenUrl;
        this.stored = stored;
    }

    public int getNumeroPokedex() {
        return numeroPokedex;
    }

    public void setNumeroPokedex(int numeroPokedex) {
        this.numeroPokedex = numeroPokedex;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getImagenUrl() {
        return imagenUrl;
    }

    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }

    public boolean isStored() {
        return stored;
    }

    public void setStored(boolean stored) {
        this.stored = stored;
    }

    @Override
    public String toString() {
        return "Pokemon{" +
                "numeroPokedex=" + numeroPokedex +
                ", nombre='" + nombre + '\'' +
                ", tipo='" + tipo + '\'' +
                ", imagenUrl='" + imagenUrl + '\'' +
                ", stored=" + stored +
                '}';
    }
}
