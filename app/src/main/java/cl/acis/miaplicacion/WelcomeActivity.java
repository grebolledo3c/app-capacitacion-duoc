package cl.acis.miaplicacion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity {

    private static final String LOG = "WelcomeActivity";

    private TextView tvMensajeBienvenida;
    private ListView lvPokemons;
    private GridView gvPokemons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        sync();

        Intent i = getIntent();

        String email = i.getStringExtra("user_email");

        String mensajeBienvenida = getString(R.string.mensaje_bienvenida);
        mensajeBienvenida += " "+email;

        this.tvMensajeBienvenida.setText(mensajeBienvenida);

        List<String> pokemons = loadPokemons();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                                            android.R.layout.simple_list_item_1, pokemons);

        this.lvPokemons.setAdapter(adapter);

        this.lvPokemons.setOnItemClickListener((v1, v2, v3, v4) -> {
            String pokemonSelected = pokemons.get(v3);
            Toast.makeText(this, "Pokemon seleccionado es: "+pokemonSelected,
                        Toast.LENGTH_LONG).show();

        });

        this.gvPokemons.setAdapter(adapter);
        this.gvPokemons.setOnItemClickListener((v1, v2, v3, v4) -> {
            String pokemonSelected = pokemons.get(v3);
            Toast.makeText(this, "Pokemon seleccionado es: "+pokemonSelected,
                    Toast.LENGTH_LONG).show();
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_cerrar_sesion:
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                 Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.menu_mostrar_lista:
                this.gvPokemons.setVisibility(View.INVISIBLE);
                this.lvPokemons.setVisibility(View.VISIBLE);
                break;
            case R.id.menu_mostrar_grilla:
                this.gvPokemons.setVisibility(View.VISIBLE);
                this.lvPokemons.setVisibility(View.INVISIBLE);
                break;
            case R.id.menu_salir:
                finish();
                System.exit(0);
        }


        return super.onOptionsItemSelected(item);
    }

    private void sync(){
        this.tvMensajeBienvenida = findViewById(R.id.tv_mensaje_bienvenida);
        this.lvPokemons = findViewById(R.id.lv_pokemons);
        this.gvPokemons = findViewById(R.id.gv_pokemons);
    }

    private List<String> loadPokemons(){
        List<String> pokemons = new ArrayList<>();
        pokemons.add("Bulbasaur");
        pokemons.add("Ivysaur");
        pokemons.add("Venusaur");
        pokemons.add("Charmander");
        pokemons.add("Charmeleon");
        pokemons.add("Charizard");
        pokemons.add("Squirtle");
        pokemons.add("Wartortle");
        pokemons.add("Blastoise");
        pokemons.add("Caterpie");
        pokemons.add("Charmeleon");
        pokemons.add("Charizard");
        pokemons.add("Squirtle");
        pokemons.add("Wartortle");
        pokemons.add("Blastoise");
        pokemons.add("Caterpie");

        return pokemons;
    }
}