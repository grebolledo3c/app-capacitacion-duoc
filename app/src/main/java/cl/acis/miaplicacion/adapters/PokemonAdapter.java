package cl.acis.miaplicacion.adapters;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cl.acis.miaplicacion.R;
import cl.acis.miaplicacion.models.Pokemon;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {

    private int layout;
    private List<Pokemon> pokemonList;
    private OnItemClickListener itemClickListener;

    public PokemonAdapter(int layout, List<Pokemon> pokemonList, OnItemClickListener listener) {
        this.layout = layout;
        this.pokemonList = pokemonList;
        this.itemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // inflar el layout (pokemon_view_item) el cual lo vamos a pasar por paramentro en el
        // constructor de la clase PokemonAdapter
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Pokemon pokemon = this.pokemonList.get(position);
        holder.bind(pokemon, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return pokemonList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvNumeroPokemon, tvNombrePokemon, tvTipoPokemon;
        public ImageView ivImagenPokemon;
        public ImageButton ibFavorito;

        private boolean check = false;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.tvNumeroPokemon = itemView.findViewById(R.id.tv_numero_pokemon);
            this.tvNombrePokemon = itemView.findViewById(R.id.tv_nombre_pokemon);
            this.tvTipoPokemon = itemView.findViewById(R.id.tv_tipo_pokemon);
            this.ivImagenPokemon = itemView.findViewById(R.id.iv_imagen_pokemon);
            this.ibFavorito = itemView.findViewById(R.id.ib_favorito);
        }

        public void bind(Pokemon pokemon, final OnItemClickListener listener){
            this.tvNumeroPokemon.setText(String.valueOf(pokemon.getNumeroPokedex()));
            this.tvNombrePokemon.setText(pokemon.getNombre());
            this.tvTipoPokemon.setText(pokemon.getTipo());
            Picasso.get().load(pokemon.getImagenUrl())
                    .placeholder(R.drawable.ic_baseline_login_24)
                    .error(R.drawable.ic_activity_bienvenida).into(this.ivImagenPokemon);

            setDrawableImagenButton(pokemon.isStored());

            ibFavorito.setOnClickListener( v -> {
                pokemon.setStored(!pokemon.isStored());
                setDrawableImagenButton(pokemon.isStored());
                listener.onItemClick(getAdapterPosition());
            });

        }

        private void setDrawableImagenButton(boolean state){
            Drawable checkDrawable;
            if (state) {
                checkDrawable = ContextCompat.getDrawable(itemView.getContext(),
                        R.drawable.ic_favorite);
            }else{
                checkDrawable = ContextCompat.getDrawable(itemView.getContext(),
                        R.drawable.ic_none_favorite);
            }
            ibFavorito.setImageDrawable(checkDrawable);
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
